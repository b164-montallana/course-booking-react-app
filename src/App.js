import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { UserProvider } from './UserContext';
import AppNavbar from './components/AppNavbar';
import CourseView from './pages/courseView';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import ErrorPage from './pages/Error';


import './App.css';


export default function App() {

//React Context is nothing but a global state to the app. It is a way to make particular data available to all the components no matter how they are nested.

  //State hook for the user state that defined here for a global scope.
  //Initialize as an object with properties from the local storage.
  //This will be used to store the user information and validation if the user is logged or not.
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })
  //Fucntion for clearing the local storage on Logout.
  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(() => {

    fetch("http://localhost:4000/api/users/details",{
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}` 
      }
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
      })
      }
    })

  }, [] )

  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
    <Router>
      <AppNavbar/>
      <Container>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/courses/:courseId" element={<CourseView />} />
          <Route path="/courses" element={<Courses />} />
          <Route path="/login" element={<Login />} />
          <Route path="/logout" element={<Logout />} />
          <Route path="/register" element={<Register />} />
          <Route path='*' element={<ErrorPage />} />
        </Routes>
      </Container>
    </Router>
    </UserProvider>  
  );
}

