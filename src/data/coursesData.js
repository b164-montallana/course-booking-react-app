const coursesData = [
    {
        id: "wdc001",
        name: "PHP-Laravel",
        description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Mollitia, aliquid? Aspernatur enim repudiandae corporis dolorem hic aliquam quam minus consequuntur in sint accusantium ea eligendi, ut illo, delectus molestias repellendus?",
        price: 45000,
        onOffer: true
    },
    {
        id: "wdc002",
        name: "Python - Django",
        description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Mollitia, aliquid? Aspernatur enim repudiandae corporis dolorem hic aliquam quam minus consequuntur in sint accusantium ea eligendi, ut illo, delectus molestias repellendus?",
        price: 50000,
        onOffer: true
    },
    {
        id: "wdc003",
        name: "JAVA - Springboot",
        description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Mollitia, aliquid?",
        price: 55000,
        onOffer: true
    }
]

export default coursesData;