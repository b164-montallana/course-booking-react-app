import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {
    return (

        <Row className='mt-3 mb-3'>
            <Col xs={12} md={4}>
                <Card className='cardHighlight p-3'>
                    <Card.Body>
                        <Card.Title>Learn From Home</Card.Title>
                        <Card.Text>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Mollitia, aliquid? Aspernatur enim repudiandae corporis dolorem hic aliquam quam minus consequuntur in sint accusantium ea eligendi, ut illo, delectus molestias repellendus?
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className='cardHighlight p-3'>
                    <Card.Body>
                        <Card.Title>Study Now, Pay Later</Card.Title>
                        <Card.Text>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Mollitia, aliquid? Aspernatur enim repudiandae corporis dolorem hic aliquam quam minus consequuntur in sint accusantium ea eligendi, ut illo, delectus molestias repellendus?
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className='cardHighlight p-3'>
                    <Card.Body>
                        <Card.Title>Be Part of Our Community</Card.Title>
                        <Card.Text>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Mollitia, aliquid? Aspernatur enim repudiandae corporis dolorem hic aliquam quam minus consequuntur in sint accusantium ea eligendi, ut illo, delectus molestias repellendus?
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>


    );
};