import { Navigate } from "react-router-dom";
import { useContext, useEffect } from "react";
import UserContext from "../UserContext";
//Redirect version5
export default function Logout() {

    const {unsetUser, setUser} = useContext(UserContext);

    unsetUser()

    useEffect( () => {
        //Set the user state back into its original value
        setUser({id: null})
    }, [])
    return (
        <Navigate to="/" />
    )

}