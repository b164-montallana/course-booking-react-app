import { Form, Button } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";


export default function Login(){

    //"useContext" hook is used to deconstruct the data of the UserContext
    const {user, setUser} = useContext(UserContext);

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isActive, setIsActive] = useState(false);

    function userLogin(e) {

        e.preventDefault();

        /*
        Syntax:
            fetch("URL", {options})
            .then(res => res.json)
            .then(data => {})
        */

        fetch('http://localhost:4000/api/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data);
            if(typeof data.accessToken !== "undefined") {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            
                Swal.fire({
                    title: "Login is successful",
                    icon: 'success',
                    text: 'Welcom to Zuitt'
                })
            } else {
                Swal.fire({
                    title: "Authentication Failed",
                    icon: 'error',
                    text: 'Check your login credentials'
                })
            }
        })

        setEmail("");
        setPassword("");

        /*
        Syntax:
            localStorage.setItem("propertyName", value)
        */
        // localStorage.setItem("email", email)

        //Set the global user state to have properties from local storage
        // setUser({
        //     email: localStorage.getItem('email')
        // })

        const retrieveUserDetails = (token) => {
            fetch('http://localhost:4000/api/users/details', {
                headers: {
                    Authorization: `Bearer ${ token }`
                }
            })
            .then(res => res.json())
            .then(data => {
                // console.log(data)

                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            })
        }

    }

    useEffect (() => {
        if(email !== "" && password !== ""){
            setIsActive(true)
        }
        else {
            setIsActive(false)
        }
    }, [email, password])

    return(
        //The page will automaticaly redirect to courses upon accessing this route.
        (user.id !== null) ?
        <Navigate to="/courses"/>
        :
        <Form onSubmit={(e) => userLogin(e)}>
        <h1 className="text-center mt-5">Login</h1>
            <Form.Group className="mb-3" controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                type="email" 
                placeholder="Enter email"
                value={email}
                onChange={e => setEmail(e.target.value)}
                required 
                />

                <Form.Text className="text-muted">
                We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>


            <Form.Group className="mb-3" controlId="userPassword">
                <Form.Label>Password </Form.Label>
                <Form.Control 
                type="password" 
                placeholder="Password"
                value={password}
                onChange={e => setPassword(e.target.value)}
                required 
                />      
            </Form.Group>
              
        {
            isActive ?
            <Button variant="danger" type="submit">
            Submit
            </Button>
            :
            <Button variant="danger" type="submit" disabled>
            Submit
            </Button>
        }
        </Form>
    )
}